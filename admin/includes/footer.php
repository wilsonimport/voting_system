<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Voting System</b>
    </div>
    <strong> &copy; <?php echo date('Y');?> Brought To You By <a href="https://hispanicoders.com">Wilson</a></strong>
</footer>