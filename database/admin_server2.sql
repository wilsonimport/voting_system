-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 25-04-2020 a las 18:42:59
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `admin_server2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `firstname`, `lastname`, `photo`, `created_on`) VALUES
(1, 'wilson', '$2y$10$IW0clhYqWG0cBqmMN21lbuWoN1p3L0Nl7RfG0.TemfKU/yR9rQsC.', 'Wilson', 'Fernandez', 'yo_chica.jpg', '2018-04-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidates`
--

CREATE TABLE `candidates` (
  `id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `platform` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `candidates`
--

INSERT INTO `candidates` (`id`, `position_id`, `firstname`, `lastname`, `photo`, `platform`) VALUES
(27, 10, 'Ivana ', 'Doover', '', ''),
(28, 10, 'Jeremy', 'Jameson', '', ''),
(29, 10, 'Steve', 'Lowenthal', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `max_vote` int(11) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `positions`
--

INSERT INTO `positions` (`id`, `description`, `max_vote`, `priority`) VALUES
(10, 'President', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `voters`
--

CREATE TABLE `voters` (
  `id` int(11) NOT NULL,
  `voters_id` varchar(15) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `state` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `voters`
--

INSERT INTO `voters` (`id`, `voters_id`, `password`, `firstname`, `lastname`, `photo`, `state`) VALUES
(19, 'AwYV5B7iNvO8xW4', '$2y$10$ceSibyLMdbFTLROgCWDYjOp7gQHT76F87fNgwZ6xMLnInQ5zCbF32', 'steve', 'Martin', '', 'OH'),
(20, 'tR5CVK2nHfQrbmL', '$2y$10$vg8piuTW16YfttCB2WnMjulU8fi2QEcAkdATN/jUO.y9m9tDoXNDK', 'Brian', 'Sanders', '', 'AZ'),
(21, '7CbTSklVyrxnDth', '$2y$10$42VCNGsZQIVy0X9INN2mxeXbAg4vKa0ZI7LEwBTYtXzJ/FGGKvRFu', 'Carter', 'Williams', '', 'AL'),
(22, 'MiPIeg1G7zcQSBJ', '$2y$10$ugMhaTHsR2X5A9rGZi00euxfgFFXgzYqSPVnbPBxT8LvrQAjydO1C', 'Jeremy', 'Sanders', '', 'AL'),
(23, 'p3R6uxHTKPECZFw', '$2y$10$9KH.H.UtcmlceWeATpT8Uu8ZTWh0k9WP5KsA2D4xa/0HJYsSAagS6', 'Steve', 'sax', '', 'AL'),
(24, 'N14XtBnPsdzFqeT', '$2y$10$n/VRTuMjzHaX4KJEnZVeqenBsk8SjYjuBCpsaj9ypoMoymmvONeFe', 'Jeremy', 'Nashville', '', 'AL'),
(25, 'rZkpeDAdLn3o4s5', '$2y$10$tDFoH2Y6t0fr2mdp/Y1aOOaZ3iuwJ2XAq.W.wWwzCYPJpllq4fa.u', 'Elsa', 'Frozen', '', 'HI'),
(26, '1pdOXetQhRk9w3Z', '$2y$10$uoZ7NeEOwUIt6JiSL7To4OwhtYQqRxsClXM04Ua6qiZPDlQXIj4F6', 'Jeremy', 'Williams', '', 'HI'),
(27, 'vwA2IXMHjJ4sCuZ', '$2y$10$UeKhMe0/nwMrTdQce/f7a.sxfSxKDjKqYV5jBaJwYmYgaD8dncPZW', 'Ivana ', 'posinova', '', 'HI'),
(28, 'RNhBoMcqbmKxIar', '$2y$10$FQIwdAF0D2eeCDZpcvyRe.xlZ2hPnLom9IdimrEEmqZS8l2t0j9M2', 'Wilson', 'Lowenthal', '', 'HI'),
(29, 'mRTL7JjiU9WYzGr', '$2y$10$zW3d.Klsh95xL4ZPGUrHzeNxC9f..6VLAXCQny5kutLAfXd72715S', 'Marcelo', 'Lastlo', '', 'FL'),
(30, 'niAYpXeBf6CIEWs', '$2y$10$CV5ukqXyRZy.mENs7HUUtuPg/NFymZj/2lUmuAeZbG/CEONgX1Mki', 'Mario', 'Santoro', '', 'FL'),
(31, 'epu89PLMyZR2hDC', '$2y$10$x/ncwfjqXJD8GL1r1oTX6eO3gWHuupASihCTHMcGiK1LDn1qCp4Zm', 'Mario', 'Andretti', '', 'FL'),
(32, 'NhdfqxGEwouIln1', '$2y$10$V8q/26yztXaRUdRb4RnE.eo1n0HB9oqNYSLUFvggycSbkXNbCPsRy', 'Natalia', 'Simionova', '', 'FL'),
(33, 'Tp39zLXHvhn8omY', '$2y$10$hsUN8J1hGS5WZM6EHYl2aORObzEW7S.uHQ91fUWz07NweTRf5C27e', 'James', 'Carlin', '', 'FL'),
(34, 'gpPix4OH6Q8YSWC', '$2y$10$HMqxsrJ9HSfMUlM38N6Ga.9S/aqktdQ4Ab3MggNACVqSoda.iaayK', 'Adrian', 'Benaro', '', 'FL'),
(35, 'L5tI6DdBo9GzU1y', '$2y$10$7kzvh2P/9ekZKpByofzLAu6J3HcxDPKJE2L0LuCGX5d.K9DBsGWBu', 'Jeremy', 'Jameson', '', 'AL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `voters_id` int(11) NOT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `position_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `votes`
--

INSERT INTO `votes` (`id`, `voters_id`, `candidate_id`, `position_id`) VALUES
(126, 16, 27, 10),
(127, 17, 28, 10),
(128, 15, 27, 10),
(129, 19, 29, 10),
(130, 20, NULL, 10),
(131, 21, 28, 10),
(132, 23, 28, 10),
(133, 24, 27, 10),
(134, 26, 29, 10),
(135, 27, 28, 10),
(136, 28, 29, 10),
(137, 29, 29, 10),
(138, 30, 27, 10),
(139, 31, NULL, 10),
(140, 32, NULL, 10),
(141, 33, 29, 10),
(142, 34, 29, 10),
(143, 35, 28, 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `voters`
--
ALTER TABLE `voters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `voters`
--
ALTER TABLE `voters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
