<footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Voting System</b>
      </div>
      <strong> &copy; <?php echo date('Y');?> Brought To You By <a href="https://hispanicoders.com">Wilson</a></strong>
    </div>
    <!-- /.container -->
</footer>